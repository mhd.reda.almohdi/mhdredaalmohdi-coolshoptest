﻿

using System.Runtime.InteropServices;

Start();


void Start()
{
    try
    {
        if (Environment.GetCommandLineArgs().Length != 4)
        {
            Console.WriteLine("Please follow these instructions:");
            Console.WriteLine("1- ./file.csv 2 Alberto");
            Console.WriteLine("2- where ./file.csv is the path to a CSV file and number 2 represents the index of the column to search in (in the previous file the name) and Alberto represents the search key.\r\n");
            return;
        }
        List<string> errors = ValidateUserInput(Environment.GetCommandLineArgs()[1], Environment.GetCommandLineArgs()[2]);
        if (errors.Count() > 0)
        {
            Console.WriteLine("Please follow these instructions:");
            foreach (var item in errors)
            {
                Console.WriteLine(item);

            }
        }
        else
        {
            Console.WriteLine(string.Join(", ", ReadRows(Environment.GetCommandLineArgs()[1], int.Parse(Environment.GetCommandLineArgs()[2]), Environment.GetCommandLineArgs()[3])));

        }

    }
    catch (Exception ex)
    {

        throw;
    }
}

static List<string> ValidateUserInput(string filePath, string columnIndex)
{
    List<string> errors = new List<string>();
    int searchIndex;

    if (!File.Exists(filePath))
    {
        errors.Add("- Enter a Valide Path");
    }

    if (!int.TryParse(columnIndex, out searchIndex))
    {

        errors.Add("- Enter a Valide Integer Column Index ");
    }

    return errors;
}
static string[] ReadRows(string filePath, int columnIndex, string searchKey)
{
    string[] recordNotFound = { "Nothing to show.." };
    try
    {
        string[] lines = File.ReadAllLines(filePath);
        for (int i = 0; i < lines.Length; i++)
        {
            string[] fields = lines[i].Split(',');
            if (RowMatch(searchKey, fields, columnIndex))
            {
                return fields;
            }
        }
        return recordNotFound;

    }
    catch (Exception ex)
    {

        return recordNotFound;

    }
}

static bool RowMatch(string searchKey, string[] fields, int columnIndex)
{
    if (fields[columnIndex].ToLower().Equals(searchKey.ToLower()))
    {
        return true;
    }
    return false;
}


